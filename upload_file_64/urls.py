from django.conf.urls import url
from .views import UploadFileView, get_encoded_file

urlpatterns = [
    url(r'^$', UploadFileView.as_view(), name='upload_file'),
    url(r'^get_encoded_file', get_encoded_file, name='get_encoded_file'),
    ]
