from django.apps import AppConfig


class UploadFile64Config(AppConfig):
    name = 'upload_file_64'
