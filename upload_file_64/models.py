from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class UploadedFile(models.Model):
    """
    Model Class that represent the uploaded file with its Meta data
    """
    created = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    upload = models.FileField(upload_to='uploads/')
    filename = models.TextField(default="")

    def __str__(self):
        return self.filename
