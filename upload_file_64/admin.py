from django.contrib import admin
from .models import UploadedFile
# Register your models here.


@admin.register(UploadedFile)
class UploadedFileAdmin(admin.ModelAdmin):
    fields = ["upload"]

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        super(UploadedFileAdmin, self).save_model(request, obj, form, change)
