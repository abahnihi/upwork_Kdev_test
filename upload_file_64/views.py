import os
import base64
from django.shortcuts import render, render_to_response
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from django.conf import settings
from django.core.files import File
from .models import UploadedFile

# Create your views here.


class UploadFileView(TemplateView):
    template_name = "upload_file.html"

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(UploadFileView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(UploadFileView, self).get_context_data(**kwargs)
        file_id = self.request.GET.get('file_id')

        if file_id:
            context['file_id'] = file_id
            file_to_send_inst = None
            try:
                file_id = int(file_id)
            except:  # need to catch some specific exceptions
                print("Not integer File ID")

            try:
                file_to_send_inst = UploadedFile.objects.get(id=file_id)
            except:  # need to catch some specific exceptions
                print("Bad File ID, May be not found")

            if file_to_send_inst:
                file_to_send_content = file_to_send_inst.upload.read()
                file_to_send_content_64 = base64.standard_b64encode(file_to_send_content)
                context['file_to_send_content'] = file_to_send_content
                context['file_to_send_content_64'] = file_to_send_content_64

        else:  # root URL that show the all files list
            all_files = UploadedFile.objects.all()
            context['all_files'] = all_files

        return context

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context=context)

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        action = request.POST.get('action')
        if action == "upload_file":
            file_content = request.POST.get('file_content')
            filename = request.POST.get('filename')
            missing_padding = len(file_content) % 4
            if missing_padding != 0:
                file_content += '=' * (4 - missing_padding)
            decoded_file = base64.b64decode(file_content)
            with open(os.path.join(settings.MEDIA_ROOT, "uploads/" + filename), 'wb+') as f:
                f.write(decoded_file)
                upload_file_inst = UploadedFile(created_by=request.user, filename=filename)
                upload_file_inst.upload.save(filename, File(f), save=False)
                upload_file_inst.save()

            if f.closed:
                os.remove(os.path.join(settings.MEDIA_ROOT, "uploads/" + filename))
                del f
            return HttpResponse("Done")

        if action == 'delete_file':
            file_id = request.POST.get('file_id')
            try:
                file_id = int(file_id)
            except:  # need to catch some specific exceptions
                print("Not integer File ID")

            try:
                file_to_delete = UploadedFile.objects.get(id=file_id)
                file_to_delete.delete()
                os.remove(file_to_delete.upload.path)  # Delete the file from the Media foler
            except:  # need to catch some specific exceptions
                print("Bad File ID, May be not found")

        return self.render_to_response(context=context)


def get_encoded_file(request):
    file_id = request.GET.get('file_id')

    if file_id:
        file_to_send_inst = None
        try:
            file_id = int(file_id)
        except:  # need to catch some specific exceptions
            print("Not integer File ID")

        try:
            file_to_send_inst = UploadedFile.objects.get(id=file_id)
        except:  # need to catch some specific exceptions
            print("Bad File ID, May be not found")

        if file_to_send_inst:
            file_to_send_content = file_to_send_inst.upload.read()
            file_to_send_content_64 = base64.b64encode(file_to_send_content)
            return HttpResponse(file_to_send_content_64)