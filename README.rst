Test Code developed for K. Dev for upwork project
=================================================

This Code is not designed for any purpose rather than just testing some coding qualification


Notes
------

* This Django project is using SQLite Database and it doesn't need any special configuration

* There is no styling in the front-end as it is for coding testing rather than produce a product

* There is no need to configure superuser nor accessing Django Admin 

* You can access the page that demonstrate from http://localhost:8000/upload/

* There are some comments within Python, HTML and JavaScript for some clarify some thoughts

* There is no implementation for logging framework. In production we ususally need it

* In front-end, we used some JavaScript ES6 syntax that need modern browser and now most of them already supporting it

* The Delete button beside each file do remove the file record from the database and remove it from the media folder as well